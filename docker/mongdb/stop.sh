#!/bin/bash

cd /product/docker/docker-compose-ymls/mongdb/config/
echo /product/mongo/17*/conf | xargs -n 1 cp -v mongod.conf
docker-compose down
rm -rf /product/mongo/17*/data/db/*
rm -rf /product/mongo/17*/logs/*


cd /product/docker/docker-compose-ymls/mongdb/shard/37/
echo /product/mongo/37*/conf | xargs -n 1 cp -v mongod.conf
docker-compose down
rm -rf /product/mongo/37*/data/db/*
rm -rf /product/mongo/37*/logs/*


cd /product/docker/docker-compose-ymls/mongdb/shard/47/
echo /product/mongo/47*/conf | xargs -n 1 cp -v mongod.conf
docker-compose down
rm -rf /product/mongo/47*/data/db/*
rm -rf /product/mongo/47*/logs/*


cd /product/docker/docker-compose-ymls/mongdb/shard/57/
echo /product/mongo/57*/conf | xargs -n 1 cp -v mongod.conf
docker-compose down
rm -rf /product/mongo/57*/data/db/*
rm -rf /product/mongo/57*/logs/*


cd /product/docker/docker-compose-ymls/mongdb/route/
echo /product/mongo/route/conf | xargs -n 1 cp -v mongod.conf
docker-compose down
rm -rf /product/mongo/route/data/db/*
rm -rf /product/mongo/route/logs/*



