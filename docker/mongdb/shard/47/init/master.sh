#!/usr/bin/env bash

echo "Do mongo DB..."

# 初始化复制集master[取自members中priority权重最大者] sh
mongo  <<EOF
use admin
var cando = false
var start = (new Date()).getTime()
while(!cando && ((new Date()).getTime() - start) < 60000){
    cando = db.isMaster().ismaster
}
if(db.isMaster().ismaster){
    db.createUser({user: 'root', pwd: 'root@mongo', roles: [{role: 'root', db: 'admin'}]})
    db.auth("root","root@mongo")
}

EOF
echo "Mongo DB do ok."
