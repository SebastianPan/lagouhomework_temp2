#!/usr/bin/env bash

echo "Init mongo Config..."

# 以配置文件重启mongod
#mongod --config /etc/mongod.conf
#mongo admin --host localhost -u root -p root@mongo

#use hi
#db.createUser({user: 'test', pwd: '123456', roles:[{role:'readWrite',db:'hi'}]})

mongo  <<EOF

use admin
var cfg={
    "_id":"shard47",
    "members":[
        {"_id":11,"host":"121.40.248.38:47011","priority":10},
        {"_id":12,"host":"121.40.248.38:47012","priority":5},
        {"_id":13,"host":"121.40.248.38:47013","arbiterOnly":true},
    ]
}
rs.initiate(cfg)
rs.status()

#db.createUser({user: 'root', pwd: 'root@mongo', roles: [{role: 'userAdminAnyDatabase', db: 'admin'}]})

EOF

#mongod  --shutdown

echo "Mongo Config init ok."
