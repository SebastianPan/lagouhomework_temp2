#!/bin/bash

cd /product/docker/docker-compose-ymls/mongdb/shard/37/
echo /product/mongo/37*/conf | xargs -n 1 cp -v mongod.conf
docker-compose down
rm -rf /product/mongo/37*/data/db/*
rm -rf /product/mongo/37*/logs/*

docker-compose up -d --build

# 数据节点启动
docker exec -it mongo37011 bash -c "mongod --config /etc/mongod.conf"
docker exec -it mongo37012 bash -c "mongod --config /etc/mongod.conf"
docker exec -it mongo37013 bash -c "mongod --config /etc/mongod.conf"
# 数据节点构建复制集
docker exec -it mongo37011 bash -c "sh /root/script/init*.sh"
docker exec -it mongo37011 bash -c "sh /root/script/master.sh"



