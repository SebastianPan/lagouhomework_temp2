#!/bin/bash

cd /product/docker/docker-compose-ymls/mongdb/route/
echo /product/mongo/route/conf | xargs -n 1 cp -v mongod.conf
docker-compose down
rm -rf /product/mongo/route/data/db/*
rm -rf /product/mongo/route/logs/*

docker-compose up -d --build

# 配置服务器启动
docker exec -it mongo_route bash -c "mongos --config /etc/mongod.conf"
docker exec -it mongo_route bash -c "sh /root/script/init*.sh"
docker exec -it mongo_route bash -c "sh /root/script/master.sh"
