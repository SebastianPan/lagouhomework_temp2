#!/usr/bin/env bash

echo "Init mongo Route..."

#mongo admin --host localhost -u root -p root@mongo

#use hi
#db.createUser({user: 'test', pwd: '123456', roles:[{role:'readWrite',db:'hi'}]})

# mongo 操作
mongo  <<EOF

use admin
db.auth("root","root@mongo")

sh.addShard("shard37/121.40.248.38:37011,121.40.248.38:37013,121.40.248.38:37013")
sh.addShard("shard47/121.40.248.38:47011,121.40.248.38:47013,121.40.248.38:47013")
sh.addShard("shard57/121.40.248.38:57011,121.40.248.38:57013,121.40.248.38:57013")
sh.status()


EOF

echo "Mongo Route init ok."
