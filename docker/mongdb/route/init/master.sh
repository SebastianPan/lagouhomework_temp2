#!/usr/bin/env bash

echo "Do mongo DB..."

# 初始化复制集master[取自members中priority权重最大者] sh
# 开启分片及指定分片键
#sh.enableSharding("you_collection")
#sh.shardCollection("you_collection.document",{"name":1,"code":"hashed"})

mongo admin -u root -p root@mongo <<EOF
use admin
var cando = false
var start = (new Date()).getTime()
while(!cando && ((new Date()).getTime() - start) < 60000){
    cando = db.isMaster().ismaster
}
if(db.isMaster().ismaster){
    db.createUser({user: 'route', pwd: 'route@mongo', roles: [{role: 'root', db: 'admin'}]})
    db.auth("root","root@mongo")
}


use lg_test
db.createCollection("tb_order")
db.createCollection("user")
db.createUser(
   {
     user: "lagou",
     pwd: "lagou@mongo",
     roles: [ {role:"readWrite", db:"lg_test"} ]
   }
)

use admin
sh.enableSharding("lg_test")
sh.shardCollection("lg_test.tb_order",{groupId: 'hashed'})
sh.shardCollection("lg_test.user",{name: 'hashed'})

use lg_test
for(var i=0;i<1000;i++){
    db.user.insert({id:i,name:"test"+i,salary:(Math.random()*20000).toFixed(2)})
}



EOF
echo "Mongo DB do ok."
