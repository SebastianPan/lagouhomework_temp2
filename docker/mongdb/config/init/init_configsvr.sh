#!/usr/bin/env bash

echo "Init mongo Config..."

mongo  <<EOF

use admin
var cfg={
    "_id":"configsvr",
    "protocolVersion" : 1,
    "members":[
        {"_id":11,"host":"121.40.248.38:17011","priority":10},
        {"_id":12,"host":"121.40.248.38:17012","priority":5},
        {"_id":13,"host":"121.40.248.38:17013","priority":5}
    ]
}
rs.initiate(cfg)
rs.status()

EOF
echo "Mongo Config init ok."
