#!/bin/bash

cd /product/docker/docker-compose-ymls/mongdb/config/
echo /product/mongo/17*/conf | xargs -n 1 cp -v mongod.conf
docker-compose down
rm -rf /product/mongo/17*/data/db/*
rm -rf /product/mongo/17*/logs/*

docker-compose up -d --build

# 配置服务器启动
docker exec -it mongo17011 bash -c "mongod --config /etc/mongod.conf"
docker exec -it mongo17012 bash -c "mongod --config /etc/mongod.conf"
docker exec -it mongo17013 bash -c "mongod --config /etc/mongod.conf"
# 配置服务器构建复制集
docker exec -it mongo17011 bash -c "sh /root/script/init*.sh"
docker exec -it mongo17011 bash -c "sh /root/script/master.sh"





