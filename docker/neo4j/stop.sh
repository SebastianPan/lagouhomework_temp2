#!/bin/bash

cd /product/docker/docker-compose-ymls/neo4j/
echo /product/neo4j/conf | xargs -n 1 cp -v neo4j.conf
docker-compose down
rm -rf /product/neo4j/data/*
rm -rf /product/neo4j/logs/*
rm -rf /product/neo4j/import/*


