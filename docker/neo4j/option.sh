#!/bin/bash

cd /product/docker/docker-compose-ymls/neo4j/
echo /product/neo4j/conf | xargs -n 1 cp -v neo4j.conf
docker-compose down
rm -rf /product/neo4j/data/*
rm -rf /product/neo4j/logs/*
rm -rf /product/neo4j/import/*


# 自定义启动
docker-compose up -d --build

# 配置服务器启动
docker exec -it neo4j bash -c "neo4j start"
#docker exec -it neo4j bash -c "sh /root/script/init*.sh"
#docker exec -it neo4j bash -c "sh /root/script/master.sh"
