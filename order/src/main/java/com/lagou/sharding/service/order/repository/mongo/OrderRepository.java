package com.lagou.sharding.service.order.repository.mongo;

import com.lagou.sharding.business.model.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends MongoRepository<Order, Long> {
    Page<Order> findByGroupNameLike(String userName, Pageable pageable);
}
