package com.lagou.sharding.service.order.service.impl;

import com.lagou.sharding.business.model.Order;
import com.lagou.sharding.business.service.OrderService;
import com.lagou.sharding.common.plugins.jwt.JwtTokenService;
import com.lagou.sharding.common.plugins.reflect.FieldUtil;
import com.lagou.sharding.common.pojo.ResultInfo;
import com.lagou.sharding.common.utils.RedisUtil;
import com.lagou.sharding.service.order.repository.mongo.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private JwtTokenService jwtTokenService;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private OrderRepository orderRepository;


    @Override
    public ResultInfo<Boolean> saveOrder(Order order) {
        orderRepository.save(order);
        return ResultInfo.success(true,order.getId().toString());
    }

    @Override
    public ResultInfo<Order> oneOder(Long orderId) {
        return ResultInfo.success(orderRepository.findById(orderId).get());
    }

    @Override
    public ResultInfo<List<Order>> findByCompanyId(Integer companyId) {
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher(FieldUtil.getFieldName(Order::getCompanyId), ExampleMatcher.GenericPropertyMatchers.contains());
        Example example = Example.of(
                    new Order().setCompanyId(companyId),
                    matcher
                );
        List<Order> orders = orderRepository.findAll(example);
        return ResultInfo.success(orders);
    }

    public ResultInfo<Page<Order>> findByUserName(String userName, Integer pageNumber, Integer pageSize){
        PageRequest pageable = PageRequest.of(pageNumber - 1,
                pageSize,
                Sort.by(Sort.Direction.ASC, FieldUtil.getFieldName(Order::getAmount)));
        return ResultInfo.success(orderRepository.findByGroupNameLike(userName, pageable));
    }
}
