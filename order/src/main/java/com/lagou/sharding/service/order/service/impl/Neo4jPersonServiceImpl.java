package com.lagou.sharding.service.order.service.impl;

import com.lagou.sharding.business.model.Person;
import com.lagou.sharding.business.service.Neo4jPersonService;
import com.lagou.sharding.service.order.repository.neo4j.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class Neo4jPersonServiceImpl implements Neo4jPersonService {
    @Autowired
    private PersonRepository personRepository;
    public List<Person> getAll(){
        List<Person>  datas = new ArrayList<>();
        personRepository.findAll().forEach(person -> datas.add(person));
        return  datas;
    }
    public  Person  save(Person person){
        return  personRepository.save(person);
    }
    public  List<Person>  personList(double money){
        return  personRepository.personList(money);
    }

    public List<Person> shortestPath(String startName,String endName){
        return personRepository.shortestPath(startName,endName);
    }
    public  List<Person> personListDept(String name){
        return  personRepository.personListDept(name);
    }


}
