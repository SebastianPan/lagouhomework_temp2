package com.lagou.sharding.service.order.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.lagou.sharding.business.model.FriendsRelation;
import com.lagou.sharding.business.model.Order;
import com.lagou.sharding.business.model.Person;
import com.lagou.sharding.business.model.User;
import com.lagou.sharding.business.service.Neo4jPersonService;
import com.lagou.sharding.business.service.OrderService;
import com.lagou.sharding.common.pojo.ResultInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private Neo4jPersonService neo4jPersonService;

    /**
     * http://127.0.0.1:8080/order/save/1000/wait-0
     * http://127.0.0.1:8080/order/save/1001/wait-1
     * @param companyId
     * @param status
     * @return
     */
    @GetMapping("/save/{companyId}/{status}")
    public ResultInfo<Boolean> save(@PathVariable Integer companyId, @PathVariable String status) {
        List<String> groups = new ArrayList<>();
        List<User> users = new ArrayList<>();
        for (int i = 0; i < RandomUtil.randomInt(2,8); i++) {
            String groupName = RandomUtil.randomEle(CollUtil.newArrayList("张", "李", "王"))
                    .concat(String.valueOf(i));
            groups.add(groupName);
            String userName = groupName + RandomUtil.randomEle(CollUtil.newArrayList("得","向","晓","明","延","雪")).concat(String.valueOf(i));
            users.add(new User()
                    .setId(IdWorker.getId())
                    .setAge(RandomUtil.randomInt(20, 80))
                    .setName(userName)
                    .setAddress("大马路"+RandomUtil.randomString(8))
                    .setMail(RandomUtil.randomNumbers(4).concat("@123.com"))
                    .setPassword(SecureUtil.md5("123456"))
                    .setVersion(1));
        }
        return orderService.saveOrder(new Order()
                .setId(IdWorker.getId())
                .setGroupId(IdWorker.getId())
                .setCompanyId(companyId)
                .setStatus(status)
                .setGroupName(RandomUtil.randomEle(groups))
                .setAmount(RandomUtil.randomBigDecimal(BigDecimal.valueOf(400),BigDecimal.valueOf(1000)))
                .setPositionId(RandomUtil.randomInt(1,10))
                .setPublishUserId(RandomUtil.randomInt(10000000,10000010))
                .setUsers(users)
                .setCreateTime(new Date())
                .setUpdateTime(DateUtil.offsetDay(new Date(), 2)));
    }

    /**
     * http://127.0.0.1:8080/order/one/1380174111419097090
     * @param orderId
     * @return
     */
    @GetMapping("/one/{orderId}")
    public ResultInfo<Order> getOne(@PathVariable Long orderId) {
        return orderService.oneOder(orderId);
    }

    /**
     * http://127.0.0.1:8080/order/list/publish/company/10000006
     * @param companyId
     * @return
     */
    @GetMapping("/list/publish/company/{companyId}")
    public ResultInfo<List<Order>> getByCompanyId(@PathVariable Integer companyId){
        return orderService.findByCompanyId(companyId);
    }

    /**
     * http://127.0.0.1:8080/order/list/publish/name/1/li
     * @param pageNumber
     * @param userName
     * @return
     */
    @GetMapping("/list/publish/name/{pageNumber}/{userName}")
    public ResultInfo<Page<Order>> findByUserName(@PathVariable Integer pageNumber, @PathVariable String userName){
        return orderService.findByUserName(userName, pageNumber, 10);
    }


    @GetMapping("/neo4j")
    public void findByUserName(){
        Person person = new Person();
        person.setName("柔骨兔");
        person.setCharacter("D");
        person.setMoney(1100);
        List<Person> personList = neo4jPersonService.personList(2600);
        FriendsRelation friendsRelation1 = new FriendsRelation().setRoles(CollUtil.newArrayList("欠款","同学父亲"))
                .setAmount(RandomUtil.randomBigDecimal(BigDecimal.valueOf(4000), BigDecimal.valueOf(8000)).setScale(2, RoundingMode.HALF_UP))
                .setPerson(RandomUtil.randomEle(personList));
        FriendsRelation friendsRelation2 = new FriendsRelation().setRoles(CollUtil.newArrayList("借款","同学朋友"))
                .setAmount(RandomUtil.randomBigDecimal(BigDecimal.valueOf(1000), BigDecimal.valueOf(4000)).setScale(2, RoundingMode.HALF_UP))
                .setPerson(RandomUtil.randomEle(personList));
        person.setPersonRoles(CollUtil.newArrayList(friendsRelation1, friendsRelation2));
        //person.setFriendsPerson(personList);
        Person p1 = neo4jPersonService.save(person);
        System.out.println(p1);
        System.out.println(neo4jPersonService.getAll());
        List<Person>  personList1 = neo4jPersonService.personList(1000);
        System.out.println(personList1);
        List<Person>  personList2 = neo4jPersonService.shortestPath("王启年","九品射手燕小乙");
        System.out.println(personList2);
        List<Person> personList3 = neo4jPersonService.personListDept("范闲");
        for (Person pe:personList3){
            System.out.println(pe);
        }
    }

}
