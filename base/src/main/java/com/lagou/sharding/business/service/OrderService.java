package com.lagou.sharding.business.service;

import com.lagou.sharding.business.model.Order;
import com.lagou.sharding.common.pojo.ResultInfo;
import org.springframework.data.domain.Page;

import java.util.List;

public interface OrderService {

    ResultInfo<Boolean> saveOrder(Order order);

    ResultInfo<Order> oneOder(Long orderId);

    ResultInfo<List<Order>> findByCompanyId(Integer companyId);

    ResultInfo<Page<Order>> findByUserName(String userName, Integer pageNumber, Integer pageSize);
}
