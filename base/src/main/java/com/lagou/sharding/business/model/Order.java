package com.lagou.sharding.business.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Document("tb_order")
public class Order implements Serializable {

    /**
     * 如果属性定义有八大基本类型 int string ...
     * Example查询需要withIgnorePaths("基础类型属性",...)
     * ExampleMatcher.matching().withIgnorePaths("age","createTime")
     */
    private Long id;
    private Boolean isDel;
    //分片键 hashed
    private Long groupId;
    private String groupName;
    private BigDecimal amount;
    private Integer companyId;
    private Integer publishUserId;
    private Integer positionId;
    //简历类型：0附件 1在线
    private Integer resumeType;
    //投递状态 WAIT-待处理理 AUTO_FILTER-自动过滤 PREPARE_CONTACT-待沟通 REFUSE-拒绝 ARRANGE_INTERVIEW-通知面试
    private String status;
    private List<User> users;
    private Date createTime;
    private Date updateTime;



}
