package com.lagou.sharding.business.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Document("tb_user")
public class User {

    private Long id;
    private String name;
    private String password;
    private Integer age;
    private String mail;
    private String address;
    private Integer version;

}

