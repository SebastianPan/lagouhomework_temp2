package com.lagou.sharding.business.model;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.RelationshipProperties;
import org.springframework.data.neo4j.core.schema.TargetNode;

import java.math.BigDecimal;
import java.util.List;

@Data
@Accessors(chain = true)
@ToString
@RelationshipProperties()
public class FriendsRelation {

    @Id
    @GeneratedValue
    private Long id;

    /**
     * 可扩关系属性
     */
    private List<String> roles;

    private BigDecimal amount;

    @TargetNode
    private Person person;

}
