package com.lagou.sharding.business.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.data.neo4j.core.schema.*;

import java.util.List;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Node("Person")
public class Person {
    @Id
    @GeneratedValue
    private Long id;
    @Property("cid")
    private Integer pid;
    @Property(name = "name")
    private String name;
    @Property(name = "age")
    private Integer age;
    @Property(name = "gender")
    private Integer gender;
    @Property(name = "character")
    private String character;
    @Property(name = "money")
    private Integer money;
    @Property(name = "description")
    private String description;

    @Relationship(type = "Friends", direction = Relationship.Direction.INCOMING)
    private List<FriendsRelation> personRoles;

    @Relationship(type = "Friends",direction = Relationship.Direction.OUTGOING)
    private List<Person> friendsPerson;

}
