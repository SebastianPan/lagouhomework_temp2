package com.lagou.sharding.business.service;

import com.lagou.sharding.business.model.Person;

import java.util.List;

public interface Neo4jPersonService {
    List<Person> getAll();
    Person  save(Person person);
    List<Person>  personList(double money);

    List<Person> shortestPath(String startName,String endName);
    List<Person> personListDept(String name);


}