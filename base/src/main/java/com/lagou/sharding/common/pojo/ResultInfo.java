package com.lagou.sharding.common.pojo;

import cn.hutool.json.JSONUtil;
import com.lagou.sharding.common.enums.ResultCode;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

@Slf4j
@Data
@Accessors(chain = true)
public class ResultInfo<T> implements Serializable {

    private Integer code;

    private String message;

    private T data;

    private String detail;

    private Boolean success;

    public Boolean isSuccess(){
        return success;
    }


    /**
     * 默认 的ResultInfo对象 msg = 成功
     *
     * @param data 默认值
     * @return
     */
    public static <T> ResultInfo<T> success(T data) {
        ResultInfo<T> resultInfo = new ResultInfo<T>();
        resultInfo.setCode(ResultCode.SUCCESS.getCode());
        resultInfo.setData(data);
        resultInfo.setMessage("成功");
        resultInfo.setSuccess(true);
        log.info("执行结果："+ JSONUtil.toJsonStr(resultInfo));
        return resultInfo;
    }

    /**
     * 默认 的ResultInfo对象 msg = 成功
     *
     * @param data 默认值
     * @return
     */
    public static <T> ResultInfo<T> success(T data, String msg) {
        ResultInfo<T> resultInfo = new ResultInfo<T>();
        resultInfo.setCode(ResultCode.SUCCESS.getCode());
        resultInfo.setData(data);
        resultInfo.setMessage(msg);
        resultInfo.setSuccess(true);
        log.info("执行结果："+ JSONUtil.toJsonStr(resultInfo));
        return resultInfo;
    }


    public static ResultInfo error(String msg) {
        ResultInfo resultInfo = new ResultInfo();
        resultInfo.setCode(ResultCode.ERROR.getCode());
        resultInfo.setSuccess(false);
        resultInfo.setMessage(msg);
        log.info("执行结果："+ JSONUtil.toJsonStr(resultInfo));
        return resultInfo;
    }

    public static ResultInfo error(String msg, String detail) {
        ResultInfo resultInfo = new ResultInfo();
        resultInfo.setCode(ResultCode.ERROR.getCode());
        resultInfo.setSuccess(false);
        resultInfo.setMessage(msg);
        resultInfo.setDetail(detail);
        log.info("执行结果："+ JSONUtil.toJsonStr(resultInfo));
        return resultInfo;
    }

    public static ResultInfo error(ResultCode code, String msg) {
        ResultInfo resultInfo = new ResultInfo();
        resultInfo.setCode(code.getCode());
        resultInfo.setSuccess(false);
        resultInfo.setMessage(msg);
        log.info("执行结果："+ JSONUtil.toJsonStr(resultInfo));
        return resultInfo;
    }

    public static ResultInfo error(ResultCode code, String msg, String detail) {
        ResultInfo resultInfo = new ResultInfo();
        resultInfo.setCode(code.getCode());
        resultInfo.setSuccess(false);
        resultInfo.setDetail(detail);
        resultInfo.setMessage(msg);
        log.info("执行结果："+ JSONUtil.toJsonStr(resultInfo));
        return resultInfo;
    }

}
